#+INFOJS_OPT:
#+PROPERTY: tangle yes

** Automata for lists
Parsing org-mode lists is an example of a non-trivial automaton. It needs to
take care of indentation, line breaks, and so on.

#+begin_src ocaml
open Prelude
open Batteries
#+end_src
In the state, we need to retain the folliwng:
#+begin_src ocaml
type state = {
#+end_src
- The items already seen
  #+begin_src ocaml
    items: Block.list_item list;
  #+end_src
- Retain whether the last line was empty (Two empty lines stops the block)
  #+begin_src ocaml
    last_line_empty: bool;
  #+end_src
- The contents of the current entry.
  #+begin_src ocaml
    current: string list;
  #+end_src
- For ordered lists, the number of the last entry and its format
  #+begin_src ocaml
     number : int option;
     format : string;
     checkbox : bool option;
  #+end_src
- Tell whether the list is ordered
  #+begin_src ocaml
     ordered : bool;
  #+end_src
#+begin_src ocaml
}
#+end_src

The following function helps computing the number of a list.
You tell it whether the list ordered, the item's format (which may be None)
and the default number inherited from the previous item, and it computes
the number of that item (as a string).
#+begin_src ocaml
  let compute_number ordered format default = 
    if not ordered then None
    else 
      Option.map_default 
        (fun format -> 
          Option.map_default (fun (x, _) -> Some x) (Some default) 
            (Numbering.fmt_extract format))
        (Some default) format
#+end_src

The following function parses the first line of a list. It returns whether the
item is numbered, the hypothetical format specified, a checkbox, and the contents of the line.

#+begin_src ocaml
  let parse_first_line s = 
    let parse_fmt s = if s.[0] = '@' then
        Some (String.sub s 1 (String.length s - 1))
      else None
    in
    let next = if s.[0] = '+' || s.[0] = '-' then Some (1, false)
      else try 
             Scanf.sscanf s "%d.%n" (fun _ k -> Some (k, true))
        with _ -> None
    in match next with
      | None -> None
      | Some (k, ordered) ->
        let module D = Delimiters.Make 
              (struct let table = ['[', (']', false)] end) in
        let open Substring in
        let s = trim (triml k (all s)) in
        match D.enclosing_delimiter s '[' with
          | None -> Some (ordered, None, None, s)
          | Some (s, rest) ->
            print_endline s;
            let b = if s = " " then Some false 
              else if s = "X" then Some true
              else None
            in
            if b = None then Some (ordered, None, parse_fmt s, rest)
            else
              match D.enclosing_delimiter (trim rest) '[' with
                | None -> Some (ordered, b, None, rest)
                | Some (fmt, rest) -> Some (ordered, b, parse_fmt fmt, rest)
#+end_src

We can know do the =is_start= function, which basically wraps the return of =parse_first_line=
into a state.
#+begin_src ocaml
  let is_start { Automaton.line } = 
    match parse_first_line line with
      | None -> None
      | Some (ordered, checkbox, format, contents) ->
        let format' = Option.default "1" format in
        Some { items = []; last_line_empty = false;
               current = [Substring.to_string contents]; 
               format = format'; checkbox; ordered;
               number = compute_number ordered format 1;
             }
    
  
#+end_src

The following function computes the items of a given state, including the item
not closed yet.
#+begin_src ocaml
  let update_current parse st = 
    {
      Block.number = Option.map (Numbering.fmt_update st.format) st.number;
      Block.contents = parse (List.enum (List.rev st.current));
      Block.checkbox = st.checkbox
    } :: st.items
    
#+end_src

The =interrupt= function: it just closes the last opened item,
and returns the total list of items.

#+begin_src ocaml
let interrupt st parse =
    [Block.List (List.rev (update_current parse st), st.ordered)]
#+end_src

The =parse_line= function is a bit more complex.
#+begin_src ocaml
  let parse_line st { Automaton.line; Automaton.parse } =
#+end_src
First it needs to take care of empty lines : two consecutive empty lines closes the block.
#+begin_src ocaml
      if line = "" then 
        if st.last_line_empty then Automaton.Done (interrupt st parse, false)
        else Automaton.Partial { st with last_line_empty = true }
#+end_src
Otherwise it checks if this line is a start of a new item
#+begin_src ocaml
      else match parse_first_line line with
        | None -> 
#+end_src
- If it's not, it checks that the line is properly indented, and adds it to the
  =current= field  
#+begin_src ocaml

          if String.starts_with line "  " then
            Automaton.Partial
              { st with current = Prelude.String.ltrim line 2 :: st.current }
          else
            Automaton.Done (interrupt st parse, false)
#+end_src
- If it is the beginning of an item, closes the current item,
  and starts a new one.
#+begin_src ocaml

        | Some (_, checkbox, format, contents) ->
          let format' = Option.default st.format format in
          let number = Option.default 0 st.number in
          Printf.printf "%d\n" number;
          let st' = { st with
            current = [Substring.to_string contents];
            format = format'; checkbox;
            number = compute_number st.ordered format (number+1);
            items = update_current parse st
          }
          in Automaton.Partial st'          
#+end_src
The priority :
#+begin_src ocaml
let priority = 10
#+end_src
