FILES=common/prelude.org common/timestamp.org common/delimiters.org common/extList.org common/numbering.org \
        syntax/inline.org syntax/context.org syntax/block.org syntax/automaton.org \
	syntax/aut_paragraph.org syntax/aut_heading.org syntax/aut_list.org \
        syntax/aut_custom_block.org syntax/parser.org

INC=-I common -I syntax
FLAGS=-package batteries -c -g $(INC)
top: $(FILES) $(FILES:.org=.cmo)
	rlwrap ocaml -init ocaml.init $(INC)
%.ocaml: %.org
	emacs --batch --visit="$^" --funcal org-babel-tangle

%.ml: %.ocaml
	mv $^ $@

doc.org: $(FILES)
	cat $(FILES) > doc.org

doc.html: doc.org
	org2html $^

doc.pdf: doc.org
	org2pdf $^

%.cmo: %.ml
	ocamlfind ocamlc $(FLAGS) -o $@ $<

clean:
	rm *cm* *ml -f

web: doc.html
	mkdir -p $(WEBDESTDIR)
	cp doc.html $(WEBDESTDIR)