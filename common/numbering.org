#+INFOJS_OPT:
#+PROPERTY: tangle yes

* Numbering
This module deals with numbering sections, lists in a general way.
#+begin_src ocaml
open Batteries
open Prelude
#+end_src
** Numbering system
A numbering system is a way to represent numbers.
#+begin_src ocaml
  type system = { encode : int -> string; 
                  decode : string -> int option * string }
  
#+end_src

Below are listed some common systems :
- based on an alphabet (digits, latin alphabet, greek alphabet)
- romain system.

Alphabetic systems are easy to implement. The function below does that. Note
that letter of the alphabet are not characters but strings as we want to use
unicode characters.

#+begin_src ocaml
    let alphabetic_sys alphabet = 
      let lg = String.length alphabet.(0) in
      let base = Array.length alphabet in
      let decode string = 
        let rec aux n k = 
          if k >= String.length string then 
            (if k = 0 then None else Some n), ""
          else
            let s = String.sub string k (String.length string - k) in
            try 
              let n' = Array.findi 
                (fun letter -> String.starts_with s letter) 
                alphabet 
              in
              aux (n * base + n') (k + lg)
            with Not_found -> 
              if k = 0 then None, s
              else Some n, s
        in aux 0 0
      in 
      let encode n = 
        let rec aux acc n = 
          if n < lg then alphabet.(n) :: acc
          else aux (alphabet.(n mod base) :: acc) (n/base)
        in
        String.concat "" (aux [] n)
      in
  { encode; decode }
  
#+end_src

Romain-type system are a bit more complex. The following only handles numbers
below 100. Note the special case of 49 which is IL.
#+begin_src ocaml
  let romain_sys [|one; five; ten; fifty; hundred|] = 
    let encode n = 
      let below_ten [one; five; ten] = function
        | 1 -> [one] | 2 -> [one; one] | 3 -> [one; one; one]
        | 4 -> [one; five] | 5 -> [five] | 6 -> [five; one]
        | 7 -> [five; one; one] | 8 -> [five; one; one; one]
        | 9 -> [one; ten] | 10 -> [ten]
        | _ -> []
      in 
      String.concat ""
        (if n = 49 then [one; fifty]
         else
            (below_ten [ten; fifty; hundred] (n/10)
             @ below_ten [one; five; ten] (n mod 10)))
    in
    let decode s = 
      let map c = List.assoc c
        [one.[0], 1; five.[0], 5; ten.[0], 10; fifty.[0], 50; hundred.[0], 100]
      in
      let rec aux n last_n k =
        if k >= String.length s then 
          (if k = 0 then None else Some n), ""
        else
          try
            let n' = map s.[k] in
            if n' > last_n then
              aux (n - last_n - last_n + n') n' (k+1)
            else
              aux (n + n') n' (k+1)
          with Not_found -> 
            (if k = 0 then None else Some n), String.ltrim s k
      in aux 0 0 0
    in {encode; decode}
#+end_src

The extendable list of numbering system :
#+begin_src ocaml
  module Systems = ExtList.Make (struct
    type t = system
    let base = [
      {encode = string_of_int;
       decode = fun s -> try Scanf.sscanf s "%d%[^\\0]" 
                               (fun n s -> Some n, s)
         with _ -> None, s};
      romain_sys [|"i"; "v"; "x"; "l"; "c"|];
      romain_sys [|"I"; "V"; "X"; "L"; "C"|];
      alphabetic_sys
        [|"a"; "b"; "c"; "d"; "e"; "f"; "g"; "h"; "i"; "j"; "k"; "l"; "m"; "n"; "o";
          "p"; "q"; "r"; "s"; "t"; "u"; "v"; "w"; "x"; "y"; "z"|];
      alphabetic_sys
        [|"A"; "B"; "C"; "D"; "E"; "F"; "G"; "H"; "I"; "J"; "K"; "L"; "M"; "N"; "O";
          "P"; "Q"; "R"; "S"; "T"; "U"; "V"; "W"; "X"; "Y"; "Z"|];
      alphabetic_sys
        [|"α"; "β"; "γ"; "δ"; "ε"; "ζ"; "η"; "θ"; "ι"; "κ"; "λ";"μ";"ν";"ξ";"ο";"π";
          "ρ";"σ";"τ";"υ";"φ";"χ";"ψ";"ω"|];
      alphabetic_sys
        [|"Α";"Β";"Γ";"Δ";"Ε";"Ζ";"Θ";"Ι";"Κ";"Λ";"Μ";"Ν";"Ξ";"Ο";"Π";"Τ";
          "Υ";"Φ";"Ψ";"Ω"|];
    ]
  end)
#+end_src

The following function parses a strings and return the corresponding number (and
the system) according to registered systems, and the rest of the string
#+begin_src ocaml
  let decode s = 
    let rec aux = function
      | [] -> None, s
      | t :: q -> match t.decode s with
          | Some n, s' -> Some (n, t), s'
          | _ -> aux q
    in aux (Systems.get ())
 #+end_src
** Formats
This section provides two functions acting on formats. A formats is a string
with a *single* representation of a number.
- The first function extracts the number in a format:
  #+begin_src ocaml
    let fmt_extract s = 
      let rec aux k = 
        if k >= String.length s then None
        else
          match fst (decode (String.ltrim s k)) with
            | Some (n, t) -> Some (n, t)
            | None -> aux (k+1)
      in aux 0
  #+end_src
- The second modifies a format to change the number in it:
  #+begin_src ocaml
    let fmt_update fmt n = 
      let rec aux start len acc k =
        if k >= String.length fmt then 
         List.rev (String.sub fmt start len :: acc)
        else
          match decode (String.ltrim fmt k) with
            | None, _ ->
              aux start (len + 1) acc (k+1)
            | Some (_, t), s' ->
              let k = String.length fmt - String.length s' in
              aux k 0 (t.encode n :: String.sub fmt start len :: acc) k
      in String.concat "" (aux 0 0 [] 0)
    
  #+end_src
